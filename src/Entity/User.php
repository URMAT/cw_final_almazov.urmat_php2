<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Article", mappedBy="userId")
     */
    private $articles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user")
     */
    private $comments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Happy", mappedBy="user")
     */
    private $happies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quality", mappedBy="user")
     */
    private $qualities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actually", mappedBy="user")
     */
    private $actuallies;

    public function __construct()
    {
        parent::__construct();
        $this->articles = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->happies = new ArrayCollection();
        $this->qualities = new ArrayCollection();
        $this->actuallies = new ArrayCollection();
        // your own logic
    }

    /**
     * @return Collection|Article[]
     */
    public function getArticles(): Collection
    {
        return $this->articles;
    }

    public function addArticle(Article $article): self
    {
        if (!$this->articles->contains($article)) {
            $this->articles[] = $article;
            $article->setUserId($this);
        }

        return $this;
    }

    public function removeArticle(Article $article): self
    {
        if ($this->articles->contains($article)) {
            $this->articles->removeElement($article);
            // set the owning side to null (unless already changed)
            if ($article->getUserId() === $this) {
                $article->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Happy[]
     */
    public function getHappies(): Collection
    {
        return $this->happies;
    }

    public function addHappy(Happy $happy): self
    {
        if (!$this->happies->contains($happy)) {
            $this->happies[] = $happy;
            $happy->setUser($this);
        }

        return $this;
    }

    public function removeHappy(Happy $happy): self
    {
        if ($this->happies->contains($happy)) {
            $this->happies->removeElement($happy);
            // set the owning side to null (unless already changed)
            if ($happy->getUser() === $this) {
                $happy->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quality[]
     */
    public function getQualities(): Collection
    {
        return $this->qualities;
    }

    public function addQuality(Quality $quality): self
    {
        if (!$this->qualities->contains($quality)) {
            $this->qualities[] = $quality;
            $quality->setUser($this);
        }

        return $this;
    }

    public function removeQuality(Quality $quality): self
    {
        if ($this->qualities->contains($quality)) {
            $this->qualities->removeElement($quality);
            // set the owning side to null (unless already changed)
            if ($quality->getUser() === $this) {
                $quality->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Actually[]
     */
    public function getActuallies(): Collection
    {
        return $this->actuallies;
    }

    public function addActually(Actually $actually): self
    {
        if (!$this->actuallies->contains($actually)) {
            $this->actuallies[] = $actually;
            $actually->setUser($this);
        }

        return $this;
    }

    public function removeActually(Actually $actually): self
    {
        if ($this->actuallies->contains($actually)) {
            $this->actuallies->removeElement($actually);
            // set the owning side to null (unless already changed)
            if ($actually->getUser() === $this) {
                $actually->setUser(null);
            }
        }

        return $this;
    }
}