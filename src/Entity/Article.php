<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @Vich\Uploadable
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $articleTitle;

    /**
     * @ORM\Column(type="string", length=2100)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="articles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoryId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $main_image;

    /**
     * @Vich\UploadableField(mapping="article_main_images", fileNameProperty="main_image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="article")
     */
    private $comments;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publicationDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tags", mappedBy="article")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Happy", mappedBy="article")
     */
    private $happies;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Quality", mappedBy="article")
     */
    private $qualities;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Actually", mappedBy="article")
     */
    private $actuallies;

    public function __construct()
    {
        $this->createDate = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->happies = new ArrayCollection();
        $this->qualities = new ArrayCollection();
        $this->actuallies = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->articleTitle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Article
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }



    public function getArticleTitle(): ?string
    {
        return $this->articleTitle;
    }

    public function setArticleTitle(string $article_title): self
    {
        $this->articleTitle = $article_title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getCategoryId(): ?Category
    {
        return $this->categoryId;
    }

    public function setCategoryId(?Category $categoryId): self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    public function getMainImage(): ?string
    {
        return $this->main_image;
    }

    public function setMainImage(?string $main_image): self
    {
        $this->main_image = $main_image;

        return $this;
    }


    /**
     * @return array
     */
    public function getComments()
    {
        return $this->comments->toArray();
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }

    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(\DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(?\DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * @return Collection|Tags[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addArticle($this);
        }

        return $this;
    }

    public function removeTag(Tags $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeArticle($this);
        }

        return $this;
    }

    /**
     * @return Collection|Happy[]
     */
    public function getHappies(): Collection
    {
        return $this->happies;
    }

    public function addHappy(Happy $happy): self
    {
        if (!$this->happies->contains($happy)) {
            $this->happies[] = $happy;
            $happy->setArticle($this);
        }

        return $this;
    }

    public function removeHappy(Happy $happy): self
    {
        if ($this->happies->contains($happy)) {
            $this->happies->removeElement($happy);
            // set the owning side to null (unless already changed)
            if ($happy->getArticle() === $this) {
                $happy->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Quality[]
     */
    public function getQualities(): Collection
    {
        return $this->qualities;
    }

    public function addQuality(Quality $quality): self
    {
        if (!$this->qualities->contains($quality)) {
            $this->qualities[] = $quality;
            $quality->setArticle($this);
        }

        return $this;
    }

    public function removeQuality(Quality $quality): self
    {
        if ($this->qualities->contains($quality)) {
            $this->qualities->removeElement($quality);
            // set the owning side to null (unless already changed)
            if ($quality->getArticle() === $this) {
                $quality->setArticle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Actually[]
     */
    public function getActuallies(): Collection
    {
        return $this->actuallies;
    }

    public function addActually(Actually $actually): self
    {
        if (!$this->actuallies->contains($actually)) {
            $this->actuallies[] = $actually;
            $actually->setArticle($this);
        }

        return $this;
    }

    public function removeActually(Actually $actually): self
    {
        if ($this->actuallies->contains($actually)) {
            $this->actuallies->removeElement($actually);
            // set the owning side to null (unless already changed)
            if ($actually->getArticle() === $this) {
                $actually->setArticle(null);
            }
        }

        return $this;
    }

}
