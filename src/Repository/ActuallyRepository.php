<?php

namespace App\Repository;

use App\Entity\Actually;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Actually|null find($id, $lockMode = null, $lockVersion = null)
 * @method Actually|null findOneBy(array $criteria, array $orderBy = null)
 * @method Actually[]    findAll()
 * @method Actually[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActuallyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Actually::class);
    }

    public function checkVotes($articleId, $userId){

        try {
            return $this->createQueryBuilder('actually')
                ->where('actually.article = :articleId')
                ->setParameter('articleId', $articleId)
                ->andWhere('actually.user = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

    }

    public function sumNumber($articleId){
        try {
            return $this->createQueryBuilder('actually')
                ->select('sum(actually.ball)')
                ->where('actually.article = :articleId')
                ->setParameter('articleId', $articleId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }
    public function sumNumberByUser($userId){
        try {
            return $this->createQueryBuilder('actually')
                ->select('sum(actually.ball)')
                ->where('actually.user = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }
}
