<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function loadByPublishDate()
    {
        return $this->createQueryBuilder('article')
            ->select('article')
            ->where('article.publicationDate IS NOT NULL')
            ->getQuery()
            ->getResult()
        ;
    }

    public function loadByPublishDateAndTags($categoryId = null)
    {
        $db = $this->createQueryBuilder('article')
            ->select('article')
            ->where('article.publicationDate IS NOT NULL')
        ;
        if(!empty($categoryId)){
            $db->andwhere('article.tags = :categoryId')
                ->setParameter('categoryId', $categoryId)
            ;
        }
        return $db->getQuery()
            ->getResult()
        ;
    }

    public function countAllRowsInArticle($categoryId = null){

        try {
            $db = $this->createQueryBuilder('article')
                ->select('count(article)')
                ->where('article.publicationDate IS NOT NULL')
            ;
            if(!empty($categoryId)){
                $db->andwhere('article.categoryId = :categoryId')
                   ->setParameter('categoryId', $categoryId)
                ;
            }
            return  $db->getQuery()
                ->getSingleScalarResult()
                ;
        } catch (NonUniqueResultException $e) {
        }
    }

    public function countAllRowsInArticleByTags($tagsId = null){

        try {
            return $this->createQueryBuilder('article')
                ->select('count(article)')
                ->where('article.publicationDate IS NOT NULL')
                ->andwhere('article.tags = :tagsId')
                ->setParameter('tadsId', $tagsId)
                ->getQuery()
                ->getSingleScalarResult()
                ;
        } catch (NonUniqueResultException $e) {
        }
    }

    public function getArticleByOffset( int $offset, int $qty, $categoryId = null)
    {
        $db = $this->createQueryBuilder('article')
            ->select('article')
            ->where('article.publicationDate IS NOT NULL')
            ;
            if(!empty($categoryId)){
                $db->andwhere('article.categoryId = :categoryId')
                    ->setParameter('categoryId', $categoryId)
                ;
            }
            return $db->orderBy('article.publicationDate', 'desc')
                ->setMaxResults($qty)
                ->setFirstResult($offset)
                ->getQuery()
                ->getResult();
    }

    public function getArticleTagsByOffset(int $offset, int $qty, $tagId = null)
    {
        return $this->createQueryBuilder('article')
            ->leftJoin('article.tags', 't')
            ->where('t = :tagId')
            ->setParameter('tagId', $tagId)
            ->orderBy('article.publicationDate', 'desc')
            ->setMaxResults($qty)
            ->setFirstResult($offset)
            ->getQuery()
            ->getResult();
    }

    public function countAllPublishArticle($userId){
        try {
            return $this->createQueryBuilder('article')
                ->select('count(article)')
                ->where('article.publicationDate IS NOT NULL')
                ->andWhere('article.userId = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }
    public function countAllUserArticle($userId){
        try {
            return $this->createQueryBuilder('article')
                ->select('count(article)')
                ->where('article.userId = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        }
    }
    public function countUserNews($user)
    {
        try {
            return $this->createQueryBuilder('n')
                ->select('SUM(q.isGood), SUM(r.isGood), COUNT(n)')
                ->innerJoin('n.qualites', 'q')
                ->innerJoin('n.relevances', 'r')
                ->where('n.author = :user')
                ->setParameter('user', $user)
                ->andWhere('n.publishDate is NOT NULL')
                ->andWhere('n.publishDate <= CURRENT_DATE()')
                ->orderBy('n.publishDate', 'DESC')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }
    public function sumNumberByUser($userId){
        try {
            return $this->createQueryBuilder('article')
                ->select('sum(qualities.ball), sum(actuallies.ball), count(article) ')
                ->innerJoin('article.qualities', 'qualities')
                ->innerJoin('article.actuallies', 'actuallies')
                ->where('article.userId = :userId')
                ->setParameter('userId', $userId)
                ->andWhere('article.publicationDate is NOT NULL')
                ->andWhere('article.publicationDate <= CURRENT_DATE()')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        };
    }
}
