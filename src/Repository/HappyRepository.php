<?php

namespace App\Repository;

use App\Entity\Happy;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Happy|null find($id, $lockMode = null, $lockVersion = null)
 * @method Happy|null findOneBy(array $criteria, array $orderBy = null)
 * @method Happy[]    findAll()
 * @method Happy[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HappyRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Happy::class);
    }

    public function checkVotes($articleId, $userId){

        try {
            return $this->createQueryBuilder('happy')
                ->where('happy.article = :articleId')
                ->setParameter('articleId', $articleId)
                ->andWhere('happy.user = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }

    }

    public function sumNumber($articleId){
        try {
            return $this->createQueryBuilder('happy')
                ->select('sum(happy.ball)')
                ->where('happy.article = :articleId')
                ->setParameter('articleId', $articleId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }
}
