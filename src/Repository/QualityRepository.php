<?php

namespace App\Repository;

use App\Entity\Quality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Quality|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quality|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quality[]    findAll()
 * @method Quality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QualityRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Quality::class);
    }

    public function checkVotes($articleId, $userId){
        try {
            return $this->createQueryBuilder('quality')
                ->where('quality.article = :articleId')
                ->setParameter('articleId', $articleId)
                ->andWhere('quality.user = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
        }
    }

    public function sumNumber($articleId){
        try {
            return $this->createQueryBuilder('quality')
                ->select('sum(quality.ball)')
                ->where('quality.article = :articleId')
                ->setParameter('articleId', $articleId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }
    public function sumNumberByUser($userId){
        try {
            return $this->createQueryBuilder('quality')
                ->select('sum(quality.ball)')
                ->where('quality.user = :userId')
                ->setParameter('userId', $userId)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NonUniqueResultException $e) {
        };
    }
}
