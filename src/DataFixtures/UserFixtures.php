<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    const USER_DATA = [
        'emails' => [
            'test@gmail.com',
            'User1@gmail.com',
            'User2@gmail.com',
            'User3@gmail.com',
            'admin@gmail.com'
            ]
        ];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::USER_DATA['emails'] as $key => $user_email) {
            $username = str_replace('@gmail.com', '', $user_email);
            $user = new User();
            $user
                ->setUsername($username)
                ->setEnabled(true)
                ->setPlainPassword('root')
                ->setEmail($user_email);
            if (self::USER_DATA['emails'][$key] === 'admin@gmail.com') {
                $user->addRole('ROLE_ADMIN');
            }
            $manager->persist($user);
            $this->addReference($user_email, $user);
        }
        $manager->flush();
    }
}