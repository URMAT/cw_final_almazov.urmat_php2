<?php


namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Tags;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagsFixtures extends Fixture
{
    const TAGS = ['Снег', 'Машина', 'Город', 'Горы', 'Бишкек', 'Сказка', 'Учеба', 'Студенты', 'Погода', 'Школа'];
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::TAGS as $key => $value) {
            $tags = new Tags();
            $tags->setTagsName($value);
            $manager->persist($tags);
            $this->addReference($value, $tags);
        }
        $manager->flush();
    }

}