<?php


namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ArticlesFixtures extends Fixture implements DependentFixtureInterface
{
    const ARTICLE = ['0', '1', '2', '3', '4'];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $description = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dicta eveniet ex illum natus nemo obcaecati odio officiis omnis ratione, repellat tempore velit, voluptas voluptate voluptatum. At culpa fuga illo iusto, labore nisi omnis pariatur quibusdam quos repellat soluta voluptas?';
        $main_photo = ['1.jpg','2.jpg','3.jpg','4.jpg','5.jpg'];
        $main_photo_2 = ['6.jpg','7.jpg','8.jpg','9.jpg','10.jpg'];
        $article_title =  ['Сочи и японский Нагато подписали соглашение о дружбе между городами',
            'При оказании помощи Китай не ставит политических условий, заявили в Пекине',
            'Конгрессмены США решили опубликовать показания по "российскому делу"',
            'В ГД оценили возможность выделения допсредств на представительские расходы',
            'Генсек ООН подтвердил Сербии, что организация не признает Косово'];
        $article_title_2 = [
            '"Аж всплакнул": в Сети не оценили пение Виталия Кличко',
            'ЕС вложит миллиард евро в развитие инфраструктуры суперкомпьютеров',
            'УФАС может возбудить дело из-за слогана Burger King',
            'Американец на велосипеде пытался покинуть Украину ради любимой в России',
            'Самолеты Ил-20 могут оборудовать новыми комплексами РЭБ'
        ];
        for($i = 0; $i <= 4; $i++){
            $article = new Article();
            $article->setArticleTitle($article_title[$i]);
            $article->setDescription($description);
            $article->setCategoryId($this->getReference(CategoriesFixtures::CATEGORIES[$i]));
            $article->setUserId($this->getReference(UserFixtures::USER_DATA['emails'][$i]));
            $article->setMainImage($main_photo[$i]);
            $article->addTag($this->getReference(TagsFixtures::TAGS[$i]));
            $article->setPublicationDate(new \DateTime());
            $manager->persist($article);
            $this->addReference($i, $article);
        }

        for($i = 0; $i <= 4; $i++){
            $article = new Article();
            $article->setArticleTitle($article_title_2[$i]);
            $article->setDescription($description);
            $article->setCategoryId($this->getReference(CategoriesFixtures::CATEGORIES[$i]));
            $article->setUserId($this->getReference(UserFixtures::USER_DATA['emails'][$i]));
            $article->setMainImage($main_photo_2[$i]);
            $article->addTag($this->getReference(TagsFixtures::TAGS[$i+4]));
            $manager->persist($article);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            UserFixtures::class,
            CategoriesFixtures::class
        ];
    }
}