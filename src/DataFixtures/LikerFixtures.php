<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 29.09.18
 * Time: 23:12
 */

namespace App\DataFixtures;





use App\Entity\Actually;
use App\Entity\Happy;
use App\Entity\Quality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LikerFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoriesFixtures::class,
            ArticlesFixtures::class,
            TagsFixtures::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

            for ($i=0; $i <= 4; $i++){
                $like = new Quality();
                $actual = new Actually();
                $happy = new Happy();
                $like
                    ->setBall(1)
                    ->setArticle($this->getReference(ArticlesFixtures::ARTICLE[$i]))
                    ->setUser($this->getReference(UserFixtures::USER_DATA['emails'][$i]));
                $actual
                    ->setBall(-1)
                    ->setArticle($this->getReference(ArticlesFixtures::ARTICLE[$i]))
                    ->setUser($this->getReference(UserFixtures::USER_DATA['emails'][$i]));
                $happy
                    ->setBall(1)
                    ->setArticle($this->getReference(ArticlesFixtures::ARTICLE[$i]))
                    ->setUser($this->getReference(UserFixtures::USER_DATA['emails'][$i]));
                $manager->persist($like);
                $manager->persist($actual);
                $manager->persist($happy);
            }
        $manager->flush();




    }
}