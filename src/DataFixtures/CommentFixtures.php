<?php
/**
 * Created by PhpStorm.
 * User: timur
 * Date: 29.09.18
 * Time: 23:12
 */

namespace App\DataFixtures;





use App\Entity\Actually;
use App\Entity\Comment;
use App\Entity\Happy;
use App\Entity\Quality;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface
{

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoriesFixtures::class,
            ArticlesFixtures::class,
            TagsFixtures::class,
        );
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i <= 4; $i++) {
            $comment = new Comment();
            $comment->setUser($this->getReference(UserFixtures::USER_DATA['emails'][$i]))
                ->setTextComment('Комментарий к статье ' . $i)
                ->setArticle($this->getReference(ArticlesFixtures::ARTICLE[$i]));
            $manager->persist($comment);
        }
        $manager->flush();
    }



}