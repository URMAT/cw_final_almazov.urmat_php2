<?php


namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoriesFixtures extends Fixture
{
    const CATEGORIES = ['Культура', 'Спорт', 'Бизнес', 'Политика', 'Экономика'];
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (self::CATEGORIES as $key => $value) {
            $category = new Category();
            $category->setCategoryName($value);
            $manager->persist($category);
            $this->addReference($value, $category);
        }
        $manager->flush();
    }

}