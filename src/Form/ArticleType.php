<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Tags;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('article_title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('imageFile', FileType::class)
            ->add('categoryId')
            ->add('tags', EntityType::class,[
                'class'=> Tags::class,
                'choice_label'=> 'tagsName',
                'multiple'=> true,
                'by_reference' => false
            ])
        ;
        $builder->add('submit', SubmitType::class, array(
            'label' => "Сохранить"
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
