<?php

namespace App\Form;

use App\Entity\Actually;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActuallyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ball', ChoiceType::class, [
                'label' => 'Голосовать по критерию Актуально/Не актуально',
                'choices' => [
                    'Качественно' => 1,
                    'Некачественно' => -1
                ]
            ])
        ;
        $builder->add('submit', SubmitType::class, array(
            'label' => "Голосовать"
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Actually::class,
        ]);
    }
}
