<?php

namespace App\Form;

use App\Entity\Quality;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QualityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ball', ChoiceType::class, [
                'label' => 'Голосовать по критерию Качественно/Некачественно',
                'choices' => [
                    'Качественно' => 1,
                    'Некачественно' => -1
                ]
            ])
        ;
        $builder->add('submit', SubmitType::class, array(
            'label' => "Голосовать"
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quality::class,
        ]);
    }
}
