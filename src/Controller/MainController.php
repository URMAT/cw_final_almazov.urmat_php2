<?php

namespace App\Controller;

use App\Entity\Actually;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Happy;
use App\Entity\Quality;
use App\Entity\Tags;
use App\Entity\User;
use App\Form\ActuallyType;
use App\Form\ArticleType;
use App\Form\CommentType;
use App\Form\HappyType;
use App\Form\QualityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $tags = $this->getDoctrine()->getRepository(Tags::class)->findAll();
        $dataByPagination = $this->pagination($request, false);

        return $this->render('main/index.html.twig', [
            'categories' => $dataByPagination['categories'],
            'articles' => $dataByPagination['articles'] ?? null,
            'pages' => $dataByPagination['pages']?? null,
            'qty' => $dataByPagination['qty']?? null,
            'tags' => $tags
        ]);
    }

    /**
     * @Route("/category/{id}", requirements={"id": "\d+"}, name="app_category")
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loadByCategoryAction(int $id, Request $request)
    {
        $tags = $this->getDoctrine()->getRepository(Tags::class)->findAll();

        $dataByPagination = $this->pagination($request, false, $id);

        return $this->render('main/index.html.twig', [
            'categories' => $dataByPagination['categories'],
            'articles' => $dataByPagination['articles'] ?? null,
            'pages' => $dataByPagination['pages']?? null,
            'qty' => $dataByPagination['qty']?? null,
            'tags' => $tags,
        ]);
    }

    /**
     * @Route("/tag/{id}", requirements={"id": "\d+"}, name="app_tags")
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loadByTagsAction(int $id, Request $request)
    {
        $tags = $this->getDoctrine()->getRepository(Tags::class)->findAll();

        $articleByTags = $this->getDoctrine()->getRepository(Tags::class)->find($id);
        $arrArticles = $articleByTags->getArticle()->toArray();

        $dataByPagination = $this->pagination($request, true, count($arrArticles));

        dump($arrArticles);


        return $this->render('main/index.html.twig', [
            'categories' => $dataByPagination['categories'],
            'articles' => $dataByPagination['articles'] ?? null,
            'pages' => $dataByPagination['pages']?? null,
            'qty' => $dataByPagination['qty']?? null,
            'tags' => $tags,
        ]);
    }

    /**
     * @Route("/add_article", name="app_add_article")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addArticleAction(Request $request)
    {
        $categories = $this->getDoctrine()
            ->getRepository('App:Category')
            ->findAll();

        $article = new Article();
        $currentUser = $this->getUser();


        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $article->setUserId($currentUser);

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute("main", [
//                "id" => $article->getId()
            ]);
        }

        return $this->render('article/add.article.html.twig', [
            'categories' => $categories,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/article/{id}", requirements={"id": "\d+"}, name="app_article_details")
     * @param int $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsArticleAction(Request $request, $id)
    {
        $categories = $this->getDoctrine()
            ->getRepository('App:Category')
            ->findAll();

        $currentUser = $this->getUser();
        $article = $this->getDoctrine()
            ->getRepository('App:Article')
            ->find($id);

        $quality = new Quality();
        $formForQuality = $this->createForm(QualityType::class, $quality);
        $formForQuality->handleRequest($request);

        $totalSumQuality = $this->getDoctrine()->getRepository(Quality::class)->sumNumber($article->getId());
        $isVotesByQuality = $this->getDoctrine()->getRepository(Quality::class)->checkVotes($article, $currentUser);

        if($request->request->has('quality') && $formForQuality->isSubmitted() && $formForQuality->isValid()){
            $quality = $formForQuality->getData();
            $quality->setUser($currentUser);
            $quality->setArticle($article);

            $em = $this->getDoctrine()->getManager();
            $em->persist($quality);
            $em->flush();

            return $this->redirectToRoute("app_article_details", [
                "id" => $article->getId()
            ]);
        }



        $actually = new Actually();
        $formForActually = $this->createForm(ActuallyType::class, $actually);
        $formForActually->handleRequest($request);

        $totalSumActually = $this->getDoctrine()->getRepository(Actually::class)->sumNumber($article->getId());
        $isVotesByActually = $this->getDoctrine()->getRepository(Actually::class)->checkVotes($article, $currentUser);

        if($request->request->has('actually') && $formForActually->isSubmitted() && $formForActually->isValid()){
            $actually = $formForActually->getData();
            $actually->setUser($currentUser);
            $actually->setArticle($article);

            $em = $this->getDoctrine()->getManager();
            $em->persist($actually);
            $em->flush();

            return $this->redirectToRoute("app_article_details", [
                "id" => $article->getId()
            ]);
        }


        $happy = new Happy();
        $formForHappy = $this->createForm(HappyType::class, $happy);
        $formForHappy->handleRequest($request);

        $totalSumHappy = $this->getDoctrine()->getRepository(Happy::class)->sumNumber($article->getId());
        $isVotesByHappy = $this->getDoctrine()->getRepository(Happy::class)->checkVotes($article, $currentUser);

        if($request->request->has('happy') && $formForHappy->isSubmitted() && $formForHappy->isValid()){
            $happy = $formForHappy->getData();
            $happy->setUser($currentUser);
            $happy->setArticle($article);

            $em = $this->getDoctrine()->getManager();
            $em->persist($happy);
            $em->flush();

            return $this->redirectToRoute("app_article_details", [
                "id" => $article->getId()
            ]);
        }



        $comment = new Comment();
        $formComment = $this->createForm(CommentType::class, $comment);
        $formComment->handleRequest($request);

        if($formComment->isSubmitted() && $formComment->isValid()){
            $comment = $formComment->getData();
            $comment->setUser($currentUser);
            $comment->setArticle($article);

            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute("app_article_details", [
                "id" => $article->getId()
            ]);
        }

        $allComments = $article->getComments();
        return $this->render('article/details.html.twig', array(
            'categories' => $categories,
            'article' => $article,
            'user' => $currentUser,
            'formComment' => $formComment->createView(),
            'comments' => $allComments,
            'formActually' => $formForActually->createView(),
            'formHappy' => $formForHappy->createView(),
            'formQuality' => $formForQuality->createView(),
            'isVotesByQuality' => $isVotesByQuality,
            'isVotesByActually' => $isVotesByActually,
            'isVotesByHappy' => $isVotesByHappy,
            'totalSumActually' => $totalSumActually,
            'totalSumHappy' => $totalSumHappy,
            'totalSumQuality' => $totalSumQuality,
        ));
    }

    /**
     * @Route("/profile/{username}", name="app_profile_user")
     * @ParamConverter("user", class="App\Entity\User")
     * @param $id
     * @param User $user
     */
    public function userProfileAction($username, User $user){

        $categories = $this->getDoctrine()
            ->getRepository('App:Category')
            ->findAll();

        $username = $user->getUsername();
        $lastLogin = $user->getLastLogin();
        $email = $user->getEmail();
        $allUserPublishArticle = $this->getDoctrine()
            ->getRepository(Article::class)
            ->countAllPublishArticle($user->getId());
        $allUserArticle = $this->getDoctrine()
            ->getRepository(Article::class)
            ->countAllUserArticle($user->getId());
        $all = array_sum($this->getDoctrine()
            ->getRepository(Article::class)
            ->sumNumberByUser($user->getId()));


        $averageRating = $all / 3;

        return $this->render('user/profile.html.twig', array(
            'categories' => $categories,
            'allUserPublishArticle' => $allUserPublishArticle,
            'allUserArticle' => $allUserArticle,
            'averageRating' => $averageRating,
            'username' => $username,
            'lastLogin' => $lastLogin,
            'email' => $email,
        ));

    }

    /**
     * @param Request $request
     * @return array
     */
    public function pagination(Request $request, bool $fromTags, $id = null )
    {
        $categories = $this->getDoctrine()
            ->getRepository('App:Category')
            ->findAll();

        if(!$fromTags){
            $numbersRowsInArticle = $this->getDoctrine()
                ->getRepository(Article::class)
                ->countAllRowsInArticle($id)
            ;
        }

        if($fromTags){
            $numbersRowsInArticle = $id;
        }


        $pages = [];
        $offset = $request->request->get('offset') ?? 0;
        $qty = $request->request->get('qty') ?? 2;
        $qtyPages = number_format(ceil($numbersRowsInArticle / $qty));

        for ($i = 1, $y = 0; $i <= $qtyPages; $i++, $y++) {
            $pages[$y]['page'] = $i;
            if ($i == (int)$offset + 1) {
                $pages[$y]['currentPage'] = $i;
            }
        }

        $offset = $offset * ((int)$qtyPages === 1 ? 0 : $qty);

        if(!$fromTags) {
            $data = $this->getDoctrine()
                ->getRepository(Article::class)
                ->getArticleByOffset($offset, $qty, $id);
        }
        if($fromTags) {
            $data = $this->getDoctrine()
                ->getRepository(Article::class)
                ->getArticleTagsByOffset($offset, $qty, $id);
        }

        return  [
            'articles' => $data,
            'categories' => $categories,
            'pages' => $pages ?? null,
            'qty' => $qty ?? null
        ];
    }
}
