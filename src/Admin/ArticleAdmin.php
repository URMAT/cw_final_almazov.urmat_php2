<?php

namespace App\Admin;

use App\Entity\Tags;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ArticleAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $this->record_id = $this->request->get($this->getIdParameter());
        $formMapper->add('articleTitle', TextType::class)
            ->add('description', TextareaType::class)
            ->add('createDate',DateTimeType::class, [])
            ->add('publicationDate',DateTimeType::class, [])
            ->add('tags', EntityType::class,[
                'class'=> Tags::class,
                'choice_label'=> 'tagsName',
                'multiple'=> true,
                'by_reference' => false
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('articleTitle');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('articleTitle');
        $listMapper->addIdentifier('description');
        $listMapper->add('_action',null,array(
            'actions' => array(
                'show'=> array(),
                'edit'=> array(),
                'delete'=> array()
            )
        ));
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show->add('articleTitle');
        $show->add('description');
    }
}